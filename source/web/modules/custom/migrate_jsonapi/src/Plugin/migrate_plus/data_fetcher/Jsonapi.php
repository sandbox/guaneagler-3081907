<?php

namespace Drupal\migrate_jsonapi\Plugin\migrate_plus\data_fetcher;

use Drupal\migrate_plus\Plugin\migrate_plus\data_fetcher\Http;
use Drupal\migrate\MigrateException;
use GuzzleHttp\Exception\RequestException;

/**
 * Retrieve data over an HTTP connection for migration.
 *
 * Example:
 *
 * @code
 * source:
 *   plugin: jsonapi
 *   data_fetcher_plugin: http
 *   headers:
 *     Accept: application/json
 *     User-Agent: Internet Explorer 6
 *     Authorization-Key: secret
 *     Arbitrary-Header: foobarbaz
 *   jsonapi_filters:
 *     groups:
 *       -
 *         key: ag
 *         conjunction: OR
 *       -
 *         key: bg
 *         conjunction: and
 *     conditions:
 *       -
 *         key: a
 *         path: value
 *         operator: STARTS_WITH
 *         value: value_a
 *         memberOf: ag
 *       -
 *         key: b
 *         path: value
 *         operator: STARTS_WITH
 *         value: value_b
 *         memberOf: ag
 *       -
 *         key: c
 *         path: value
 *         operator: STARTS_WITH
 *         value: value_c
 *         memberOf: bg
 *       -
 *         key: d
 *         path: value
 *         operator: STARTS_WITH
 *         value: value_d
 * @endcode
 *
 * @DataFetcher(
 *   id = "jsonapi",
 *   title = @Translation("JSON:API")
 * )
 */
class Jsonapi extends Http {

  /**
   * {@inheritdoc}
   */
  public function getResponse($url) {
    try {

      $options = ['headers' => $this->getRequestHeaders()];
      if (!empty($this->configuration['authentication'])) {
        $options = array_merge($options, $this->getAuthenticationPlugin()->getAuthenticationOptions());
      }
      $response = $this->httpClient->get($url, $options);
      if (empty($response)) {
        throw new MigrateException('No response at ' . $url . '.');
      }
    }
    catch (RequestException $e) {
      throw new MigrateException('Error message: ' . $e->getMessage() . ' at ' . $url . '.');
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getResponseContent($url) {
    $response = $this->getResponse($url);
    return $response->getBody();
  }

}
