<?php

namespace Drupal\migrate_jsonapi\Plugin\migrate\source;

use Drupal\migrate_plus\Plugin\migrate\source\Url;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Urls for jsonapi.
 *
 * @MigrateSource(
 *   id = "jsonapi",
 *   title = @Translation("JSONAPI")
 * )
 */
class Jsonapi extends Url {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    if (!isset($configuration['urls'])) {
      $configuration['urls'] = '';
    }
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

}
