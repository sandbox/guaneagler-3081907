vcl 4.0;

import std;

/**
 * Example VCL for Authcache Varnish / Authcache ESI
 * =================================================
 *
 * See also core.vcl for detailed information.
 *
 * Credits & Sources
 * -----------------
 * * Josh Waihi - Authenticated page caching with Varnish & Drupal:
 *   http://joshwaihi.com/content/authenticated-page-caching-varnish-drupal
 * * Four Kitchens - Configure Varnish 3 for Drupal 7:
 *   https://fourkitchens.atlassian.net/wiki/display/TECH/Configure+Varnish+3+for+Drupal+7
 * * The Varnish Book:
 *   https://www.varnish-software.com/static/book/
 * * The Varnish Book - VCL Request Flow:
 *   https://www.varnish-software.com/static/book/_images/vcl.png
 */

/**
 * Define all your backends here.
 */
backend default {
    .host = "{projectKey}-web-server";
    .port = "80";
}

acl purge {
  "{projectKey}-web-server";
  "localhost";
  "127.0.0.1";
}

/**
 * Include Authcache Varnish core.vcl.
 */
include "/usr/local/etc/varnish/core.vcl";

/**
 * Defines where the authcache varnish key callback is located.
 *
 * Note that the key-retrieval path must start with a slash and must include
 * the path prefix if any (e.g. on multilingual sites or if Drupal is installed
 * in a subdirectory).
 */
sub authcache_key_path {
  set req.http.X-Authcache-Key-Path = "/authcache-varnish-get-key";

  // Example of a multilingual site relying on path prefixes.
  # set req.http.X-Authcache-Key-Path = "/en/authcache-varnish-get-key";

  // Example of a drupal instance installed in a subdirectory.
  # set req.http.X-Authcache-Key-Path = "/drupal/authcache-varnish-get-key";
}

/**
 * Derive the cache identifier for the key cache.
 */
sub authcache_key_cid {
  if (req.http.Cookie ~ "(^|;)\s*S?SESS[a-z0-9]+=") {
    // Use the whole session cookie to differentiate between authenticated
    // users.
    set req.http.X-Authcache-Key-CID = "sess:"+regsuball(req.http.Cookie, "^(.*;\s*)?(S?SESS[a-z0-9]+=[^;]*).*$", "\2");
  }
  else {
    // If authcache key retrieval was enforced for anonymous traffic, the HTTP
    // host is used in order to keep apart anonymous users of different
    // domains.
    set req.http.X-Authcache-Key-CID = "host:"+req.http.host;
  }

  /* Optional: When using authcache_esi alongside with authcache_ajax */
  // if (req.http.Cookie ~ "(^|;)\s*has_js=1\s*($|;)") {
  //   set req.http.X-Authcache-Key-CID = req.http.X-Authcache-Key-CID + "+js";
  // }
  // else {
  //   set req.http.X-Authcache-Key-CID = req.http.X-Authcache-Key-CID + "-js";
  // }

  /* Optional: When serving HTTP/HTTPS */
  // if (req.http.X-Forwarded-Proto ~ "(?i)https") {
  //   set req.http.X-Authcache-Key-CID = req.http.X-Authcache-Key-CID + "+ssl";
  // }
  // else {
  //   set req.http.X-Authcache-Key-CID = req.http.X-Authcache-Key-CID + "-ssl";
  // }
}

/**
 * Place your custom vcl_recv code here.
 */
sub authcache_recv {
  # Do not cache these paths.
  if (req.url ~ "^/status\.php$" ||
      req.url ~ "^/update\.php" ||
      req.url ~ "^/install\.php" ||
      req.url ~ "^/apc\.php$" ||
      req.url ~ "^/admin" ||
      req.url ~ "^/admin/.*$" ||
      req.url ~ "^/user" ||
      req.url ~ "^/user/.*$" ||
      req.url ~ "^/users/.*$" ||
      req.url ~ "^/info/.*$" ||
      req.url ~ "^/flag/.*$" ||
      req.url ~ "^.*/ajax/.*$" ||
      req.url ~ "^.*/ahah/.*$" ||
      req.url ~ "^/system/files/.*$") {

    return (pass);
  }

  # Always cache the following file types for all users. This list of extensions
  # appears twice, once here and again in vcl_fetch so make sure you edit both
  # and keep them equal.
  if (req.url ~ "(?i)\.(asc|dat|txt|doc|xls|ppt|tgz|csv|png|gif|jpeg|jpg|ico|swf|css|js)(\?.*)?$") {
    unset req.http.Cookie;
  }

  /**
    * Example 2: Remove all but
    * - the session cookie (SESSxxx, SSESSxxx)
    * - the cache invalidation cookie for authcache p13n (aucp13n)
    * - the NO_CACHE cookie from the Bypass Advanced module
    * - the nocache cookie from authcache
    *
    * Note: Please also add the has_js cookie to the list if Authcache Ajax
    * is also enabled in the backend. Also if you have Authcache Debug enabled,
    * you should let through the aucdbg cookie.
    *
    * More information on:
    * https://www.varnish-cache.org/docs/3.0/tutorial/cookies.html
    */
  if (req.http.Cookie) {
    set req.http.Cookie = ";" + req.http.Cookie;
    set req.http.Cookie = regsuball(req.http.Cookie, "; +", ";");
    set req.http.Cookie = regsuball(req.http.Cookie, ";(S?SESS[a-z0-9]+|aucp13n|NO_CACHE|nocache|has_js|drupalauth4ssp|SimpleSAMLSessionID|Drupal\.authcache\.aucdbg|Drupal\.authcache\.cache_render|Drupal\.visitor\.user_access_page|Drupal\.visitor\.absorb_relay_state|aucdbg)=", "; \1=");
    set req.http.Cookie = regsuball(req.http.Cookie, ";[^ ][^;]*", "");
    set req.http.Cookie = regsuball(req.http.Cookie, "^[; ]+|[; ]+$", "");

    if (req.http.Cookie == "") {
     unset req.http.Cookie;
    }
  }

  # Check the incoming request type is "PURGE", not "GET" or "POST".
  if (req.method == "PURGE") {
    # Check if the IP is allowed.
    if (!std.ip(regsub(req.http.X-Forwarded-For, "[, ].*$", ""), client.ip) ~ purge) {
      # Return error code 405 (Forbidden) when not.
      return (synth(405, "Not allowed."));
    }
    return (purge);
  }
}

sub vcl_backend_fetch {
  // /**
  //  * Example 1: Use a passphrase to validate proxy requests.
  //  *
  //  * The standard Drupal way to verify whether a request came in via a proxy
  //  * is to compare the X-Forwarded-For header to a whitelist. By default
  //  * Authcache Varnish uses the same method. This fails however, if this
  //  * check is carried out by the webserver (e.g., when using Nginx with the
  //  * realip module).
  //  *
  //  * In that case, set a passphrase on the request and configure the same in
  //  * settings.php, e.g.:
  //  *
  //  *    $conf['authcache_varnish_passphrase'] = 'correct horse battery staple';
  //  *
  //  */
  // if (bereq.http.X-Authcache-Get-Key != "skip") {
  //   set bereq.http.X-Authcache-Varnish-Passphrase = "correct horse battery staple";
  // }
}

sub vcl_backend_response {
  // TODO: Place your custom fetch policy here

  // /* Example 1: Cache 404s, 301s, 500s. */
  // if (beresp.status == 404 || beresp.status == 301 || beresp.status == 500) {
  //   set beresp.ttl = 10 m;
  // }

  // /*
  //  * Example 2: Do not cache when backend specifies Cache-Control: private,
  //  * no-cache or no-store.
  //  */
  // if (req.esi_level == 0 && beresp.http.Cache-Control ~ "(private|no-cache|no-store)") {
  //   set beresp.ttl = 0s;
  // }
}

sub vcl_deliver {
  // TODO: Modify response, add / remove headers
  // /**
  //  * Example 1: Disable browser cache in Safari.
  //  *
  //  * @see:
  //  *   - https://bugs.webkit.org/show_bug.cgi?id=71509
  //  *   - https://groups.drupal.org/node/191453
  //  *   - https://drupal.org/node/1910178
  //  */
  // if (resp.http.X-Generator ~ "Drupal" && req.http.user-agent ~ "Safari" && req.http.user-agent !~ "Chrome") {
  //   set resp.http.Cache-Control = "no-cache, must-revalidate, post-check=0, pre-check=0";
  // }

   /**
    * Example 2: Add a hit-miss header to the response.
    *
    * See:
    * https://www.varnish-cache.org/trac/wiki/VCLExampleHitMissHeader
    */
   if (obj.hits > 0) {
     set resp.http.X-Varnish-Cache = "HIT";
   }
   else {
     set resp.http.X-Varnish-Cache = "MISS";
   }
}

sub vcl_hash {
  # ...
  if (req.http.X-Forwarded-Proto &&
      req.url !~ "(?i)\.(png|gif|jpeg|jpg|ico|gz|tgz|bz2|tbz|mp3|ogg|zip|rar|otf|ttf|eot|woff|svg|pdf)$") {
     hash_data(req.http.X-Forwarded-Proto);
  }
  # ...
}
