version: '2'

networks:
  default-network:
    external:
      name: ${PROJECT_GROUP_NAME}_default-network

services:
  web-server:
    image: ciandtchina/drupal-web:php${PHP_VERSION}-apache-devpack
    volumes:
      - ../source:/var/www/html

  database:
    extends:
      file: docker-compose.common.yml
      service: database
    environment:
      - MYSQL_PASSWORD=3.1415926
      - MYSQL_ROOT_PASSWORD=3.1415926
