# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|

  config.vm.box = "ubuntu/trusty64"

  config.vm.box_check_update = false

  config.vm.network  "private_network", type: "dhcp"

  config.vm.hostname  = "##PROJECT_KEY##"

  if !Vagrant::Util::Platform.windows? || Vagrant.has_plugin?("vagrant-winnfsd") then
    config.vm.synced_folder "..", "/vagrant", type: "nfs"
  else
    config.vm.synced_folder "..", "/vagrant"
  end

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.memory = "2048"
  end

  config.vm.provision 'Installing Docker runtime', type: 'shell', inline: 'curl -fsSL https://get.docker.com -o get-docker.sh && sh get-docker.sh --mirror Aliyun && rm get-docker.sh'

  config.vm.provision 'Adding user vagrant to docker group', type: 'shell', inline: 'usermod -aG docker vagrant'

  config.vm.provision 'Adding Docker mirror for image fast downloading', type: 'shell', inline: 'echo \'DOCKER_OPTS="$DOCKER_OPTS --registry-mirror=https://lfmqdbt1.mirror.aliyuncs.com"\' >> /etc/default/docker && service docker restart'
  
  config.vm.provision 'Installing Docker Compose', type: 'shell', inline: 'curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && chmod +x /usr/local/bin/docker-compose'

  config.vm.provision 'Installing Bindfs', type: 'shell', inline: "apt-get install -y bindfs"

  if !Vagrant::Util::Platform.windows? then
    current_user=%x( id -u | tr -d '\n' )
    current_group=%x( id -g | tr -d '\n' )
    bindfs_create_for="--create-for-user=#{current_user} --create-for-group=#{current_group}"
    additional_args=""
  else
    bindfs_create_for=""
    additional_args="--windows-hack"
  end

  config.vm.provision 'Binding /vagrant to /home/vagrant/source', type: 'shell', run: 'always', inline: <<-SHELL
      set -e
      sudo mkdir -p /home/vagrant/source
      sudo bindfs -u vagrant -g vagrant -o nonempty --chown-ignore --chgrp-ignore --chmod-normal #{bindfs_create_for} /vagrant /home/vagrant/source
  SHELL

  if Vagrant::Util::Platform.windows? then
    config.vm.provision 'Fix shell files line ending caused by git-for-windows autocrlf', type: 'shell', run: 'always', inline: <<-SHELL
        set -e
        sed -i 's/\r$//g' /home/vagrant/source/devops/scripts/.common_func
        sed -i 's/\r$//g' /home/vagrant/source/devops/scripts/*.sh
    SHELL
  end

  config.vm.provision 'Checking if composer install required', type: 'shell', inline: <<-SHELL
      set -e
      . /home/vagrant/source/devops/.env
      [ ! -z ${COMPOSER_FOLDER_PATH} ] && /home/vagrant/source/devops/scripts/composer.sh #{additional_args} install
  SHELL

  config.vm.provision 'Booting project Docker environment', type: 'shell', inline: '/home/vagrant/source/devops/scripts/docker.sh start vagrant', run: 'always'

  config.vm.provision 'Fix /home/vagrant permission', type: 'shell', :inline => 'chown vagrant.vagrant -R /home/vagrant'

  if Vagrant.has_plugin?("vagrant-hostmanager") then
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = false
    config.hostmanager.include_offline = false
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.aliases = %w(##PROJECT_KEY##.vagrant)

    config.hostmanager.ip_resolver = proc do |machine|
      if machine.provider.to_s =~ /^VirtualBox.*$/
      then
        interface = 'eth1'
      else
        interface = 'eth0'
      end
      result = ""
      machine.communicate.execute("ifconfig " + interface) do |type, data|
        result << data if type == :stdout
      end
      (ip = /^\s*inet .*?(\d+\.\d+\.\d+\.\d+)\s+/.match(result)) && ip[1]
    end
  end
end
