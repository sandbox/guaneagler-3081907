PROJECT_GROUP_NAME=TEST

# COMPOSE_PROJECT_NAME Can be same as project key
COMPOSE_PROJECT_NAME=test
PROJECT_KEY=test

# PHP version, 7.1 or 7.2
PHP_VERSION=7.2

# Mysql version, all version numbers can be found here: https://hub.docker.com/r/library/mysql/tags/
MYSQL_VERSION=5.6
MYSQL_DB_NAME=test
MYSQL_USER=test

# The relevant path for the composer.json file, usually it's the "source" directory
COMPOSER_FOLDER_PATH=source

# The relevant path of the node project, or theme directory built with webpack
NODE_FOLDER_PATH=
