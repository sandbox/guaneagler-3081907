<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Drupal\DrupalExtension\Context\RawDrupalContext;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext
{

    protected $screenshot_error_dir = '/tmp';
    protected $screenshot_result_dir = '/tmp';
    protected $environment;
    protected $parameters;
    protected $currentFeature;
    protected $currentScenario;
    /**
     * Initializes context.
     */
    public function __construct($parameters)
    {
        $extra_param_arr = $this->getParametersByJson($parameters);
        $this->parameters = $parameters = array_merge($parameters, $extra_param_arr);
        //print_r($parameters);
        if (isset($parameters['base_path']) && isset($parameters['site_key'])) {
            $screenshot_base_path = $parameters['base_path'] . '/screenshots/' . $parameters['site_key'];
            $this->screenshot_error_dir = $screenshot_base_path . '/error';
            $this->screenshot_result_dir = $screenshot_base_path . '/result';
        }
    }

    //Get parameters of JSON file configuration
    public function getParametersByJson($param)
    {
        $file_name = $param['base_path'] . '/Extra_parameters.json';
        if (file_exists($file_name)) {
            $json_string = file_get_contents($file_name);
            $data = json_decode($json_string, true);
            return $data;
        } else {
            return [];
        }
    }

    /**  Sets the base URL for all environments.
     * @param string $url * The url to set.
     * @see: https://github.com/Behat/MinkExtension/issues/155#issuecomment-77041296
     */
    private function setBaseUrl($url)
    {
        foreach ($this->environment->getContexts() as $context) {
            if ($context instanceof \Behat\MinkExtension\Context\RawMinkContext) {
                $context->setMinkParameter('base_url', $url);
            }
        }
    }

    /**
     * Take screenshot when step fails. Works only with Selenium2Driver.
     *  @AfterStep
     */
    public function afterStep(Behat\Behat\Hook\Scope\AfterStepScope $scope)
    {
        $testResult = $scope->getTestResult();
        if (!$testResult->isPassed()) {
            //if dir not exists,will create dir
            $dir = $this->screenshot_error_dir . '/' . $this->currentFeature;
            is_dir($dir) || @mkdir($dir, 0777, true);

            $driver = $this->getSession()->getDriver();

            if ($driver instanceof Behat\Mink\Driver\Selenium2Driver) {
                $fileName = $this->currentScenario . '.png';
                $this->saveScreenshot($fileName, $dir);

                $screenshotFilePath = $dir . '/' . $fileName;
                print 'Screenshot at: ' . $screenshotFilePath;

                // Append the screenshot path to exception
                if ($testResult->hasException()) {
                    $exception = $testResult->getException();
                    $reflectionObject = new ReflectionObject($exception);
                    $reflectionObjectProp = $reflectionObject->getProperty('message');
                    $reflectionObjectProp->setAccessible(true);
                    $reflectionObjectProp->setValue($exception, $exception->getMessage() . " - Screenshot saved to {$screenshotFilePath}");
                }
            }
        }
    }

    /**
     *
     * @BeforeScenario
     */
    public function beforeScenario(Behat\Behat\Hook\Scope\BeforeScenarioScope $scope)
    {
        // Load and save the environment for each scenario.
        $this->environment = $scope->getEnvironment();
        // Set the base URL. Can be overridden check @see.
        $base_url = $this->parameters['base_url'];
        $this->setBaseUrl($base_url);
        //get current Scenarios
        $this->currentScenario = preg_replace('/\W/', '', $scope->getScenario()->getTitle());
        //get current Feature
        $this->currentFeature = preg_replace('/\W/', '', $scope->getFeature()->getTitle());
    }

    /**
     * Take screenshot after the scenarios. Works only with Selenium2Driver.
     * @AfterScenario
     */
    public function afterScenario(Behat\Behat\Hook\Scope\AfterScenarioScope $scope)
    {
        //if dir not exists,will create dir
        $dir = $this->screenshot_result_dir . '/' . $this->currentFeature;
        is_dir($dir) || @mkdir($dir, 0777, true);

        $driver = $this->getSession()->getDriver();
        if ($driver instanceof Behat\Mink\Driver\Selenium2Driver) {
            $fileName = $this->currentScenario . '.png';
            $this->saveScreenshot($fileName, $dir);
            print 'Screenshot at: ' . $dir . '/' . $fileName;
        }
    }

    /**
     * @When /^(?:|I )should see "([^"]*)" in popup$/
     *
     * @param string $message The message.
     * @return bool
     */
    public function assertPopupMessage($message)
    {
        return $message == $this->getSession()->getDriver()->getWebDriverSession()->getAlert_text();
    }

    /**
     * @when /^(?:|I )confirm the popup$/
     */
    public function confirmPopup()
    {
        $this->getSession()->getDriver()->getWebDriverSession()->accept_alert();
    }

    /**
     * Set a waiting time in seconds
     * @When I wait :seconds second(s)
     */
    public function waitForSeconds($seconds)
    {
        sleep($seconds);
    }

    /**
     * Clicking the element with selector(works only if element is visible)
     * @When I click on :selector
     */
    public function clickOnElement($selector)
    {
        $node = $this->getSession()->getPage()->find('css', $selector);
        if ($node) {
            $node->click();
        } else {
            throw new Exception('Element not found');
        }
    }

    /** Scroll the page to the bottom
     * @Then I scroll page to bottom
     */
    public function ScrollToBottom()
    {
        $response = $this->getSession()->getDriver()->evaluateScript(
            "
        return (function () {
            var h = jQuery(document).height()-jQuery(window).height();
            jQuery(document).scrollTop(h);
            return true;
        })();
        "
        );
    }

    /** Scroll the page to the top
     * @Then I scroll page to top
     */
    public function ScrollToTop()
    {
        $response = $this->getSession()->getDriver()->evaluateScript(
            "
        return (function () {
            jQuery(document).scrollTop(0,0);
            return true;
        })();
        "
        );
    }

    /** set value for form field
     * @When I fill :selector with :value
     */
    public function FillWith($selector, $value)
    {
        $node = $this->getSession()->getPage()->find('css', $selector);
        if ($node) {
            $node->setValue($value);
        } else {
            throw new Exception('Element not found');
        }
    }

    /** Selects option in select field with javascript selector
     * And I select option "100" for ".custom-excess-select"
     * @When I select option :value for :selector
     */
    public function SelectOptionFor($value, $selector)
    {
        $node = $this->getSession()->getPage()->find('css', $selector);
        if ($node) {
            $node->selectOption($value);
        } else {
            throw new Exception('Element not found');
        }
    }

    /**
     * Clicks link with specified id|title|alt|text
     */
    public function ClickLink($link)
    {
        $link = $this->fixStepArgument($link);
        $this->getSession()->getPage()->clickLink($link);
    }

    /**
     * Fills in form field with specified id|name|label|value
     */
    public function FillField($field, $value)
    {
        $field = $this->fixStepArgument($field);
        $value = $this->fixStepArgument($value);
        $this->getSession()->getPage()->fillField($field, $value);
    }

    /**
     * Returns fixed step argument (with \\" replaced back to ")
     * @param string $argument
     * @return string
     */
    public function FixStepArgument($argument)
    {
        return str_replace('"', '', str_replace('\\"', '"', $argument));
    }

    /**
     * Selects option in select field with specified id|name|label|value
     */
    public function SelectOptions($select, $option)
    {
        $select = $this->fixStepArgument($select);
        $option = $this->fixStepArgument($option);
        $this->getSession()->getPage()->selectFieldOption($select, $option);
    }

    /**
     * Check that element with specified CSS contains specified text
     */
    public function ElementContainsText($element, $text)
    {
        $this->assertSession()->elementTextContains('css', $element, $this->fixStepArgument($text));
    }

    /**
     * Check that element with specified CSS doesn't exist on page
     */
    public function ElementNotOnPage($element)
    {
        $this->assertSession()->elementNotExists('css', $element);
    }

    /**
     * Presses button with specified id|name|title|alt|value
     */
    public function PressButton($button)
    {
        $button = $this->fixStepArgument($button);
        $this->getSession()->getPage()->pressButton($button);
    }

    /**
     * Find elements based on the JS selector
     */
    public function FindOptions($selector)
    {
        $node = $this->getSession()->getPage()->find('css', $selector);
        if ($node) {
            return $node;
        } else {
            throw new Exception('Element not found,' . $selector);
        }
    }

    /**
     * Returns element's value by js selector
     */
    public function GetValues($selector)
    {
        return $this->FindOptions($selector)->getValue($selector);
    }

    /**
     * Returns element's text by js selector
     */
    public function GetText($selector)
    {
        return $this->FindOptions($selector)->getText($selector);
    }

    /**
     * Checks whether checkbox checked located by js selector
     */
    public function IsChecked($selector)
    {
        return $this->FindOptions($selector)->isChecked($selector);
    }

    /**
     * Remove the spaces in the string
     */
    public function RemoveSpaces($str)
    {
        return str_replace(' ', '', $str);
    }

    /**
     * Returns element's attribute by js selector.
     */

    public function GetAttribute($seletor, $attr_name)
    {
        $attribute = $this->FindOptions($seletor)->getAttribute($attr_name);
        if ('' !== $attribute) {
            return $attribute;
        }
    }

    /**
     * Switches to specific iFrame by element class name.
     * @When I switch to iframe :class
     */
    public function SwitchToIframe($class)
    {
        $function = <<<JS
            (function(){
                 var frames = document.getElementsByTagName('iframe');
                    for (var i=0; i < frames.length; i++) {
                        if (frames[i].className == "$class") {
                            frames[i].name = "iframeToSwitchTo";
                        }
                    }
            })()
JS;
        try {
            $this->getSession()->executeScript($function);
        } catch (Exception $e) {
            print_r($e->getMessage());
            throw new Exception("Element $class was NOT found." . PHP_EOL . $e->getMessage());
        }
        try {
            //$this->getSession()->executeScript($function);
            $this->getSession()->getDriver()->switchToIFrame("iframeToSwitchTo");
        } catch (Exception $e) {
            throw new Exception(sprintf("switch iframe caught exception: %s", $e->getMessage()));
        }
    }

    /**
     * Switches to specific browser window.
     * @When I switch to :name window
     *
     */
    public function switchToWindow($name)
    {
        if ($name = 'default') {$name = null;} //(default for switching back to main window)
        $this->getSession()->getDriver()->switchToWindow($name ? $name : '');
    }

    /**
     * Presses button with specified id|name|title|alt|value and confirm alert popup with message :message
     * Example: When I press "Log In" and confirm alert popup with message "hello world"
     *
     * @When /^(?:|I )press "(?P<button>(?:[^"]|\\")*)" and confirm alert popup with message "([^"]*)"$/
     */
    public function pressButtonAndConfirmAlertMessage($button, $message)
    {
        // Press the button
        $button = $this->fixStepArgument($button);
        $this->getSession()->getPage()->pressButton($button);

        // Get the alert message and accept
        $alert_message = $this->getSession()->getDriver()->getWebDriverSession()->getAlert_text();
        $this->getSession()->getDriver()->getWebDriverSession()->accept_alert();

        return $alert_message == $message;
    }

    /**
     * Scrolling to the particular element(arg - element's selector to scroll to)
     *
     * @When /^I scroll page to element "([^"]*)"$/
     */
    public function iScrollToElement($arg)
    {
        $function = <<<JS
     var scrollContainer = jQuery('body, html').offset().top;
         scrollBlock = jQuery('$arg').offset().top;
         Container = jQuery('body, html');
 jQuery('body, html').scrollTop(scrollBlock - scrollContainer+Container.scrollTop());

JS;
        try {
            $this->getSession()->executeScript($function);
        } catch (Exception $e) {
            throw new \Exception("ScrollIntoElement failed:" . $e->getMessage());
        }
    }

}
