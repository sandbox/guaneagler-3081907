Create Behat tests for your project
======

## Background
1. The default implementation of the Behat tests are run in docker containers, please check the container details(behat and browser) in docker-compose.yml file. 
2. If you want to run the Behat scripts in your local environment, be sure you have the Docker installed, and you can run docker-compose commands.


## Start to write Behat feature/scenario
1. Check with your behat.yml file and make necessary changes according to your projects, especially the base_url in your project profile
2. Create .feature files under the features/your_project directory.
3. Include "@javascript" above your scenario if you want the Behat testing can execute javascript. Otherwise the testing will be browser headless.


## Start/Stop the docker containers
Before running your Behat scripts, you need to start up your docker containers with following command.
```
docker-compose up -d
```
Once you finish you testing, you can run following command to stop your running containers.
```
docker-compose stop
```

## Execute your Behat scripts
1. During the local testing or debug your scripts, usually we would run the testing for a specific feature, see an example command below

```
./run-behat features/covermore/quotebox.feature --profile aus --format=html 
```

2. If you want to run the testing with all your features, see another example below
```
./run-behat --profile aus --format=junit
```